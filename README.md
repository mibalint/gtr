# gtr - Google Translate CLI and associated throttling daemon.

gtr is a python3 package and cli interface for google translate. 

## Installation

Install just like any other package in your python3 environment:

```bash
pip install -r requirements.txt
```

## Usage

```bash
TODO
```

## Development with a virtualenv

```bash
pip install -r dev-requirements.txt
pytest
```

## Development with docker compose - TODO

```bash
TODO
```
