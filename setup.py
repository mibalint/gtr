# -*- coding: utf-8 -*
"""
Setup script for gtr, the google translate cli and daemon.

USAGE:
    pip install -r dev-requirements.txt
    pytest
"""
import sys
import os.path
HERE0 = os.path.dirname(__file__) or os.curdir
os.chdir(HERE0)
HERE = os.curdir
sys.path.insert(0, HERE)

from setuptools import find_packages, setup

# -----------------------------------------------------------------------------
# CONFIGURATION:
# -----------------------------------------------------------------------------
python_version = float('%s.%s' % sys.version_info[:2])
requirements = [
    'Baker',
    'requests[security]',
    'daemonize',
    'ratelimit',
]

README = os.path.join(HERE, 'README.md')
description = ''.join(open(README).readlines())
PROVIDES = ['gtr']

# -----------------------------------------------------------------------------
# UTILITY:
# -----------------------------------------------------------------------------
def find_packages_by_root_package(packages):
    """
    Better than excluding everything that is not needed,
    collect only what is needed.
    """
    all_packages = []
    for p in packages:
        where = os.path.join(HERE, p)
        root_package = os.path.basename(where)
        all_packages.append(root_package)
        all_packages.extend(
            '%s.%s' % (root_package, sub_package)
            for sub_package in find_packages(where))
    return all_packages


# -----------------------------------------------------------------------------
# SETUP:
# -----------------------------------------------------------------------------
setup(
    name='gtr',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description='gtr - Google translate cli and associated throttling daemon',
    long_description=description,
    author='Mihai Balint',
    author_email='balint.mihai@gmail.com',
    url='http://bitbucket.com/mibalint/gtr',
    provides=PROVIDES,
    packages=find_packages_by_root_package(PROVIDES),
    include_package_data=True,
    py_modules=[],
    scripts=['bin/gtd', 'bin/gtranslate'],
    entry_points={
        'console_scripts': [],
        'distutils.commands': [],
    },
    install_requires=requirements,
    tests_require=['pytest >= 3.6.3'],
    cmdclass={
    },
    extras_require={},
    use_2to3=False,
    license='BSD',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Developers',
        'Topic :: Office/Business',
    ],
    zip_safe=True,
)
