"""
Very basic subprocess based unix socket server and client. Message processing
is based on reading entire lines of (unicode) text from clients, creating a
single subprocess to handle each line and returning the subprocess output as
the response.
"""
import os
import socket
import subprocess
import sys
import threading

from ratelimit import limits, sleep_and_retry
from gtr.abstract_sock import SocketServer


class Server(SocketServer):

    def __init__(self, command, socket_path=None):
        super().__init__(socket_path)
        self.command = command

    def sock_init(self):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.bind(self.socket_path)
        self.sock.listen()

    def sock_close(self):
        self.sock.close()

    def sock_loop(self):
        if not self.blocking:
            threading.Thread(target=self.accept_loop, args=[]).start()
            return
        self.accept_loop()
        self._safe_exit()

    def accept_loop(self):
        while self.running:
            try:
                client_sock, client_address = self.sock.accept()
                client_sock.settimeout(200)
                threading.Thread(
                    target=self.client_worker, args=[client_sock]).start()
            except:
                if self.running:
                    raise

    def client_worker(self, client_sock):
        sf = client_sock.makefile()
        try:
            while self.running:
                query = sf.readline()
                self.client_loop(client_sock, query)
        finally:
            client_sock.close()

    @sleep_and_retry
    @limits(calls=int(os.environ.get('QUERIES_PER_SEC', '10')), period=1)
    def client_loop(self, client_sock, query):
        proc = subprocess.Popen(
            self.command, stdin=subprocess.PIPE, stdout=client_sock)
        try:
            proc.communicate(input=query.encode(), timeout=5)
        finally:
            proc.kill()
            proc.communicate()

    def socket_fds(self):
        return [self.sock.fileno()]


class Client(object):
    def __init__(self, socket_path=None):
        self.socket_path = socket_path or 'gtd.sock'
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.connect(self.socket_path)
        self.sf = self.sock.makefile()

    def send(self, query):
        self.sock.sendall(f'{query}\n'.encode())
        return self.sf.readline().strip('\n')
