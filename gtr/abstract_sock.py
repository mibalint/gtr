"""
Abstract socket server. This only implements the generic server socket
lifecycle methods.
"""
import abc
import os
import signal


class SocketServer(metaclass=abc.ABCMeta):

    def __init__(self, socket_path=None):
        self.socket_path = socket_path or 'gtd.sock'
        self.running = False
        self.blocking = True
        self._cleanup()
        self.sock_init()

    @abc.abstractmethod
    def socket_fds(self):
        """Return the file descriptors used by this server"""
        pass

    @abc.abstractmethod
    def sock_init(self):
        """Initialize the unix socket"""
        pass

    @abc.abstractmethod
    def sock_close(self):
        """Close the unix socket"""
        pass

    @abc.abstractmethod
    def sock_loop(self):
        """Handle incoming connection through the unix socket"""
        pass
    
    def _cleanup(self):
        try:
            os.unlink(self.socket_path)
        except FileNotFoundError:
            pass

    def _safe_exit(self):
        try:
            self.sock_close()
        except:
            pass
        self._cleanup()
        print(f'\nExiting.')

    def start(self, blocking=True):
        signal.signal(signal.SIGINT, self.kill_received)
        signal.signal(signal.SIGTERM, self.kill_received)
        self.running = True
        self.blocking = blocking
        self.sock_loop()

    def stop(self):
        self.kill_received()
        
    def kill_received(self, *args, **kwargs):
        self.running = False
        if not self.blocking:
            self._safe_exit()
