"""Language detection and translation API model and implementations.
"""
import abc
import os
import requests


class Translator(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def detect(self, text: str):
        """Detect the text language. Returns ISO-639-1 string identifying \
        the language. Raises TranslationException when language detection \
        fails."""
        pass

    @abc.abstractmethod
    def translate(self, text: str, target: str):
        """Translates the given text into the target lanugage. Returns a \
        string with the translation. Raises TranslationException when \
        translation fails."""
        pass


class TranslationException(Exception):
    pass


class GoogleHttpTranslator(Translator):
    API_ROOT = 'https://translation.googleapis.com/language/translate/v2'
    # TODO: consider moving into it's own module

    def __init__(self, api_key=None):
        # Get an API key from the GCP console:
        # https://console.cloud.google.com/apis/credentials
        self.api_key = api_key or os.environ.get('GC_API_KEY')
        if not self.api_key:
            raise TranslationException(
                'Google Cloud Translate API key is required. '
                'Pass it in or set GC_API_KEY environment variable.')

    def _upstream(self, api, q, **kwargs):
        assert api in ['/detect', '/']
        url = f'{self.API_ROOT}{api}'
        params = {
            'q': q,
            'format': 'text',
            'key': self.api_key,
        }
        params.update(kwargs)
        response = requests.post(f'{self.API_ROOT}{api}', params=params)
        if not response.ok:
            code = response.status_code
            message = response.json().get('error').get('message')
            raise TranslationException(f'Error: {code} - {message}')
        return response.json()

    def detect(self, text):
        # see https://cloud.google.com/translate/docs/reference/detect
        response = self._upstream('/detect', text)
        detections = response['data']['detections']
        if len(detections) == 1:
            return detections[0][0]['language']
        reliable, reliable_confidence = None, 0
        best, best_confidence = None, 0
        # Google will return the confidence of each detection as well as
        # a reliability boolean
        for dl in detections:
            d = dl[0]
            if d['confidence'] > best_confidence:
                best_confidence = d['confidence']
                best = d['language']
            if d['isReliable'] and d['confidence'] > reliable_confidence:
                    reliable_confidence = d['confidence']
                    reliable = d['language']
        # prioritize the reliable result oover the the highest confidence
        # and for simplicity, return a single response
        result = reliable or best
        if result is None:
            raise TranslationException()
        return result

    def translate(self, text, target):
        # see https://cloud.google.com/translate/docs/reference/translate
        response = self._upstream('/', text, target=target)
        translations = response['data']['translations']
        if not translations:
            raise TranslationException()
        # For simplicity, return only the first translation
        return translations[0]['translatedText']
