"""Language detection and translation API. Provides a basic and easy to use
python interface with support for multiple translation backends.

Available backends:
* Google Cloud Translate (using the REST API)

"""
from gtr.model import Translator, TranslationException, GoogleHttpTranslator


TRANSLATOR_FACTORIES = {
    'default': GoogleHttpTranslator,
    'gtr': GoogleHttpTranslator,
}


def make_translator(key=None, *args, **kwargs):
    return TRANSLATOR_FACTORIES.get(key or 'default')(*args, **kwargs)


def register_translator(key, factory, default=False):
    TRANSLATOR_FACTORIES[key] = factory
    if default:
        TRANSLATOR_FACTORIES['default'] = factory
