"""
Translator daemon and client implementations using various processing models:
* AIO Unix Socket
* [TODO] Subproceses
"""
import json

import gtr.aiosock
import gtr.procsock
import gtr


class TranslatorAIOServer(object):

    def __init__(self, socket_path=None, translator=None):
        self.server = gtr.aiosock.Server(self._on_message, socket_path)
        translator = gtr.make_translator(translator)
        self.protocol = {
            'detect': translator.detect,
            'translate': translator.translate,
        }

    async def _on_message(self, cid, query):
        return _on_message(self.protocol, cid, query)


class TranslatorAIOClient(gtr.Translator):

    def __init__(self, socket_path=None):
        self.client = gtr.aiosock.Client(socket_path)

    def _upstream(self, method, **kwargs):
        query = {
            'method': method,
            'args': kwargs,
        }
        response = json.loads(self.client.send(json.dumps(query)))
        if response['status'] == 'error':
            raise gtr.TranslationException(response['error'])
        return response['result']

    def detect(self, text):
        return self._upstream('detect', text=text)

    def translate(self, text, target):
        return self._upstream('translate', text=text, target=target)


class TranslatorMPServer(object):

    def __init__(self, socket_path=None, translator=None):
        self.server = gtr.procsock.Server(
            ['gtd', 'translate', translator or 'default'], socket_path)


def _on_message(protocol, cid, query):
    try:
        result = _on_message_object(protocol, cid, json.loads(query))
        return json.dumps({'status': 'ok', 'result': result})
    except gtr.TranslationException as e:
        return json.dumps({'status': 'error', 'error': str(e)})
    except:
        return json.dumps({'status': 'error', 'error': 'json parse error'})


def _on_message_object(protocol, cid, query_object):
    method = protocol.get(query_object['method'])
    return method(**query_object['args'])


def translate_work(translator_key, query):
    translator = gtr.make_translator(translator_key)
    protocol = {
        'detect': translator.detect,
        'translate': translator.translate,
    }
    print(_on_message(protocol, 0, query))
