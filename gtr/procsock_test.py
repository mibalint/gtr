
import os
import pytest
from datetime import datetime

import gtr.procsock as procsock


def test_server_start_stop():
    server = procsock.Server('cat')
    assert len(server.socket_fds()) > 0
    server.start(blocking=False)
    
    client = procsock.Client()    
    assert client.send('Hello there  ') == 'Hello there  '
    assert client.send('What''s your name?') == 'What''s your name?'    
    server.stop()

def measure(calls, rps):
    os.environ['QUERIES_PER_SEC'] = f'{rps}'
    server = procsock.Server('cat')
    server.start(blocking=False)

    client = procsock.Client()

    start = datetime.now()
    for rep in range(calls):
        msg = f'Hello {rep}.'
        assert client.send(msg) == msg
    duration = datetime.now() - start
    server.stop()
    return duration.total_seconds()


def test_server_rate_limit():
    # We only test with the default 10 rps limit to showcase
    # that limits happen, changing the rps to something else
    # is more... involved
    m1 = measure(30, 10)
    assert m1 > 2.0 and m1 < 4.0

    m2 = measure(10, 10)
    assert m2 > 0.5 and m2 < 2.0

    m2 = measure(25, 10)
    assert m2 > 1.5 and m2 < 3.5
