"""

"""
import pytest

import gtr
import gtr.gtdaemon as gtd


class MockTranslator(gtr.Translator):
    MOCK_EN_RO = {
        'Hello world': 'Salutare lume',
        'What''s your name': 'Cum te cheama',
        'cat': 'pisică',
        'dog': 'caine',
        'car': 'masina',
        'test': 'test',
    }

    def __init__(self):
        self.mock_data = {
            'en_ro': dict(MockTranslator.MOCK_EN_RO),
            'ro_en': {v: k for k, v in MockTranslator.MOCK_EN_RO.items()}
        }

    def detect(self, text):
        result = None
        for tr_key, tr_map in self.mock_data.items():
            if text in tr_map.keys():
                if result is not None:
                    # ambiguous, can't decide so fail
                    raise gtr.TranslationException()
                result = tr_key.split('_')[0]
        if result is None:
            raise gtr.TranslationException()
        return result

    def translate(self, text, target):
        result = None
        for tr_key, tr_map in self.mock_data.items():
            if tr_key[3:] == target:
                result = tr_map.get(text)
        if result is None:
            raise gtr.TranslationException()
        return result


@pytest.fixture
def translator():
    gtr.register_translator('mock', MockTranslator, default=True)
    return gtr.make_translator()


@pytest.fixture
def gtr_translator():
    return gtr.make_translator('gtr')


def test_gtr_detect(gtr_translator):
    assert gtr_translator.detect('cat') == 'en'


def test_gtr_translate(gtr_translator):
    assert gtr_translator.translate('cat', 'ro') == 'pisică'


def test_detect(translator):
    assert translator.detect('cat') == 'en'
    assert translator.detect('pisică') == 'ro'


def test_ambiguous_detect(translator):
    with pytest.raises(gtr.TranslationException):
        translator.detect('test')


def test_translate(translator):
    assert translator.translate('cat', 'ro') == 'pisică'
    assert translator.translate('caine', 'en') == 'dog'


def test_translator_aioserver(translator):
    ts = gtd.TranslatorAIOServer()
    ts.server.start(blocking=False)

    tc = gtd.TranslatorAIOClient()
    assert tc.translate('cat', 'ro') == 'pisică'
    assert tc.translate('caine', 'en') == 'dog'
    assert tc.detect('cat') == 'en'
    with pytest.raises(gtr.TranslationException):
        tc.detect('test')
    ts.server.stop()


def test_translator_mpserver():
    # The subprocess execution model isn't easily testable using
    # mock objects, so we test with the default translator
    ts = gtd.TranslatorMPServer()
    ts.server.start(blocking=False)

    tc = gtd.TranslatorAIOClient()
    assert tc.translate('cat', 'ro') == 'pisică'
    assert tc.translate('caine', 'en') == 'dog'
    assert tc.detect('cat') == 'en'
    ts.server.stop()
