"""

"""
import pytest

import gtr.aiosock as aiosock


async def echo(cid, query):
    return f'{cid} - {query}'

def test_server_start_stop():
    server = aiosock.Server(echo)
    assert len(server.socket_fds()) > 0
    server.start(blocking=False)
    server.stop()

def test_send_one_message():
    server = aiosock.Server(echo)
    server.start(blocking=False)
    client = aiosock.Client()
    assert client.send('Hello World') == '0 - Hello World'
    server.stop()

def test_send_multiple_messages():
    server = aiosock.Server(echo)
    server.start(blocking=False)
    client = aiosock.Client()
    assert client.send('Hello there  ') == '0 - Hello there  '
    assert client.send('What''s your name?') == '0 - What''s your name?'
    assert client.send('My name is Hal9000') == '0 - My name is Hal9000'
    server.stop()

def test_multiple_client_send():
    server = aiosock.Server(echo)
    server.start(blocking=False)
    c1 = aiosock.Client()
    c2 = aiosock.Client()
    c3 = aiosock.Client()
    assert c1.send('Hello there') == '0 - Hello there'
    assert c2.send('What''s your name?') == '1 - What''s your name?'
    assert c3.send('My name is Hal9000') == '2 - My name is Hal9000'
    assert c2.send('My name is Dave') == '1 - My name is Dave'
    server.stop()
